import { NgModule } from '@angular/core';

import { ClientHomeComponent } from './client-home/client-home.component';
import { ClientDetailComponent } from './client-detail/client-detail.component';
import { SharedModule } from '../shared/shared.module';
import { ClientRoutingModule } from './client-routing.module';
import { ClientListComponent } from './components/client-list/client-list.component';
import { ClientTableComponent } from './components/client-table/client-table.component';
import { ClientNewComponent } from './components/client-new/client-new.component';

@NgModule({
  imports: [
    SharedModule,
    ClientRoutingModule
  ],
  declarations: [ClientHomeComponent, ClientDetailComponent, ClientListComponent, ClientTableComponent, ClientNewComponent],
  entryComponents: [ClientNewComponent]
})
export class ClientModule { }
