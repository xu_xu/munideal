import { Component, OnInit } from '@angular/core';

export interface Client {
  name: string;
  position: number;
  jan: number;
  feb: number;
  mar: number;
  apr: number;
  may: number;
  june: number;
  july: number;
  aug: number;
  sept: number;
  oct: number;
  nov: number;
  dec: number;
}

const ELEMENT_DATA: Client[] = [
  {position: 1, name: 'Hydrogen', jan: 1089.7, feb: 0, mar: 0, apr: 0, may: 0, june: 0, july: 0, aug: 800, sept: 0, oct: 0, nov: 0, dec: 0},
  {position: 2, name: 'Helium', jan: 0, feb: 0, mar: 0, apr: 0, may: 0, june: 0, july: 0, aug: 0, sept: 0, oct: 0, nov: 0, dec: 34467},
  {position: 3, name: 'Lithium', jan: 0, feb: 0, mar: 0, apr: 0, may: 0, june: 0, july: 0, aug: 7678, sept: 0, oct: 0, nov: 0, dec: 0},
  {position: 4, name: 'Beryllium', jan: 0, feb: 0, mar: 0, apr: 0, may: 0, june: 0, july: 0, aug: 0, sept: 0, oct: 0, nov: 0, dec: 0},
  {position: 5, name: 'Boron', jan: 0, feb: 0, mar: 0, apr: 4546, may: 0, june: 0, july: 0, aug: 0, sept: 0, oct: 0, nov: 0, dec: 5200},
  {position: 6, name: 'Carbon', jan: 0, feb: 0, mar: 0, apr: 0, may: 0, june: 0, july: 8848, aug: 0, sept: 0, oct: 0, nov: 0, dec: 3200},
  {position: 7, name: 'Nitrogen', jan: 0, feb: 0, mar: 7889, apr: 0, may: 0, june: 0, july: 0, aug: 0, sept: 0, oct: 0, nov: 0, dec: 8000},
  {position: 8, name: 'Oxygen', jan: 0, feb: 0, mar: 0, apr: 0, may: 0, june: 0, july: 0, aug: 20001, sept: 1298, oct: 0, nov: 0, dec: 0},
  {position: 9, name: 'Fluorine', jan: 0, feb: 0, mar: 0, apr: 0, may: 0, june: 0, july: 0, aug: 0, sept: 0, oct: 0, nov: 0, dec: 8000},
  {position: 10, name: 'Neon', jan: 0, feb: 22335, mar: 0, apr: 0, may: 0, june: 0, july: 0, aug: 0, sept: 0, oct: 123342, nov: 0, dec: 0},
];

@Component({
  selector: 'app-client-table',
  templateUrl: './client-table.component.html',
  styleUrls: ['./client-table.component.scss']
})
export class ClientTableComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'jan', 'feb', 'mar', 'apr', 'may', 'june', 'july', 'aug', 'sept', 'oct', 'nov', 'dec'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit() {
  }

}
