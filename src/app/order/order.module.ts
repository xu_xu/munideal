import { NgModule } from '@angular/core';
import { OrderHomeComponent } from './order-home/order-home.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { SharedModule } from '../shared/shared.module';
import { OrderRoutingModule } from './order-routing.module';
import { OrderListComponent } from './components/order-list/order-list.component';
import { OrderNewComponent } from './components/order-new/order-new.component';

@NgModule({
  imports: [
    SharedModule,
    OrderRoutingModule
  ],
  declarations: [OrderHomeComponent, OrderDetailComponent, OrderListComponent, OrderNewComponent]
})
export class OrderModule { }
